class _GraphNode:
    '''Rappresenta un nodo del grafo. Da usarsi solo all'interno di Graph.'''
    def __init__(self, name, adj, info):
        self.name = name
        self.adj = set(adj)
        self.info = info

class Graph:
    '''Rappresenta un grafo'''
    def __init__(self):
        '''Inizializza un grafo vuoto.'''
        self._nodes = {}
    def addNode(self, name, info=None):
        '''Aggiunge un nodo name, se non esiste'''
        if name  in self._nodes: return
        self._nodes[name] = _GraphNode(name, set(), info)
    def addEdge(self, name1, name2):
        '''Aggiunge un arco che collega i nodi name1 e name2'''
        if not (name1 in self._nodes and name2 in self._nodes): return 
        self._nodes[name1].adj.add(name2)
    def adjacents(self, name):
        '''Ritorna una lista dei nomi dei nodi adiacenti al nodo name,
        se il nodo non esiste, ritorna None'''
        if name in self._nodes:
            app=list(self._nodes[name].adj)
            app.sort()
            return app
        return None
    def nodes(self):
        '''Ritorna una lista dei nomi dei nodi del grafo'''
        return list(self._nodes.keys())
    def edges(self):
        '''Ritorna una lista degli archi  del grafo'''
        edges = set()
        for name, node in self._nodes.items():
            for adj in node.adj:
                if (adj, name) in edges: continue
                edges.add((name,adj))
        return list(edges)
    def nodeInfo(self, name):
        '''Ritorna l'info del nodo name, se il nodo non esiste, ritorna None'''
        if name in self._nodes: return self._nodes[name].info
        return None
    def printGraph(self):
        '''Stampa il grafo'''
        for i in self.nodes():
            print(i, " -> ", self.adjacents(i))
    



graph = Graph()
for i in range(5):
    graph.addNode(i)


graph.addEdge(0,1)
graph.addEdge(0,4)
graph.addEdge(1,2)
graph.addEdge(1,3)
graph.addEdge(4,3)


visited=len(graph.nodes())*[0]
padre=len(graph.nodes())*[-1]
cc=len(graph.nodes())*[-1]


def has_cycle():
    '''Restituisce true se il grafo contiene un ciclo, false altrimenti'''
    global visited
    for i in graph.nodes():
        if(DFS_cycle(i)):
            visited=len(graph.nodes())*[0]
            return True
    visited=len(graph.nodes())*[0]
    return False

def DFS_cycle(n):
    '''Metodo d'appoggio, puo essere utilizzato per vedere se esiste un ciclo visitando a partire dal nodo n'''
    visited[n]=1
    for i in graph.adjacents(n):
        if(not visited[i]):
            if(DFS_cycle(i)):
                return True
        elif(visited[i]==1):
            return True
    visited[n]=-1
    return False
    


spazi=-4
def DFS_tree(n):
    '''Metodo per stampare il grafo sotto forma di albero (non mostra i cicli)'''
    global spazi
    spazi+=4
    visited[n]=1
    print(spazi*" ",n)
    for i in graph.adjacents(n):
        if(not visited[i]):
            DFS_tree(i)
    spazi-=4

stack=[]
def ord_topologico():
    '''Metodo che restituisce un ordinamento topologico'''
    global visited
    if(has_cycle()):
        print("ciclo trovato, ordinamento topologico impossibile")
        return 
    app=[]
    for i in graph.nodes():
        if(not visited[i]):
            DFS_ord_topologico(i)
            app.extend(stack)
            stack.clear()
    app.reverse()
    print(app)
        
        
   

def DFS_ord_topologico(n):
    '''Metodo d'appoggio ordinamento topologico'''
    visited[n]=1
    for i in graph.adjacents(n):
        if(not visited[i]):
            DFS_ord_topologico(i)
    stack.append(n)   



active=len(graph.nodes())*[0]

def SCC_components():
    '''Metodo che stampa le componenti fortemente connesse'''
    for i in graph.nodes():
        if(not visited[i]):
            DFS_SCC_components(i)
app=[]            
next_id=0
ll=len(graph.nodes())*[False]
def DFS_SCC_components(n):
    '''Metodo d'appoggio componenti fortemente connesse'''
    global next_id
    stack.append(n)
    visited[n]=ll[n]=next_id=next_id+1
    active[n]=True
    for i in graph.adjacents(n):
        if(not visited[i]):
            padre[i]=n
            DFS_SCC_components(i)
            ll[n]=min(ll[n],ll[i])
        elif(active[i]):
            ll[n]=min(ll[n],visited[i])            
    active[n]=False
    if(ll[n]==visited[n]):
        w=stack.pop()
        app.append(w)
        while(n!=w):
            w=stack.pop()
            app.append(w)
        app.reverse()
        print(app)
        app.clear()            
    


        
avanti=[]
indietro=[]
attraversamento=[]
def DFS_archi(n):
    '''Metodo che calcola archi in avanti, indietro e di attraversamento,
    il risultato va a finire nelle liste 'avanti', 'indietro' e 'attraversamento' '''
    visited[n]=-1
    for i in graph.adjacents(n):
        if(visited[i]==-1):
            indietro.append((n,i))
        elif(visited[i]==1):
            attraversamento.append((n,i))
        else:
            avanti.append((n,i))
            DFS_archi(i)
    visited[n]=1
