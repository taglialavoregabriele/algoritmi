import queue
class _GraphNode:
    '''Rappresenta un nodo del grafo. Da usarsi solo all'interno di Graph.'''
    def __init__(self, name, adj, info):
        self.name = name
        self.adj = set(adj)
        self.info = info

class Graph:
    '''Rappresenta un grafo'''
    def __init__(self):
        '''Inizializza un grafo vuoto.'''
        self._nodes = {}
    def addNode(self, name, info=None):
        '''Aggiunge un nodo name, se non esiste'''
        if name  in self._nodes: return
        self._nodes[name] = _GraphNode(name, set(), info)
    def addEdge(self, name1, name2):
        '''Aggiunge un arco che collega i nodi name1 e name2'''
        if not (name1 in self._nodes and name2 in self._nodes): return 
        self._nodes[name1].adj.add(name2)
        self._nodes[name2].adj.add(name1)
    def adjacents(self, name):
        '''Ritorna una lista dei nomi dei nodi adiacenti al nodo name,
        se il nodo non esiste, ritorna None'''
        if name in self._nodes:
            return list(self._nodes[name].adj)
        return None
    def nodes(self):
        '''Ritorna una lista dei nomi dei nodi del grafo'''
        return list(self._nodes.keys())
    def edges(self):
        '''Ritorna una lista degli archi  del grafo'''
        edges = set()
        for name, node in self._nodes.items():
            for adj in node.adj:
                if (adj, name) in edges: continue
                edges.add((name,adj))
        return list(edges)
    def nodeInfo(self, name):
        '''Ritorna l'info del nodo name, se il nodo non esiste, ritorna None'''
        if name in self._nodes: return self._nodes[name].info
        return None
    def printGraph(self):
        for i in self.nodes():
            print(i, " -> ", self.adjacents(i))



#inizializzazione del grafo
graph = Graph()
for i in range(12):
    graph.addNode(i)
graph.addEdge(0,1)
graph.addEdge(2,1)
graph.addEdge(4,5)
graph.addEdge(5,6)
graph.addEdge(6,7)
graph.addEdge(7,5)
graph.addEdge(9,8)
graph.addEdge(10,9)
graph.addEdge(11,10)
graph.addEdge(11,8)
graph.printGraph()


#LISTE UTILI
ris=[]
visited=len(graph.nodes())*[0]
active=len(graph.nodes())*[0]
parent=len(graph.nodes())*[-1]



spazi=0
def DFS_tree(u):
    '''DFS per stampare l'albero'''
    global spazi
    print(" "*spazi,u)
    visited[u]=1
    for i in graph.adjacents(u):
        if(not visited[i]):
            spazi+=4
            DFS_tree(i)
    spazi-=4

    


def DFS_iterativa(n):
    '''semplice DFS iterativa'''
    visited[n]=True
    ris=[]
    ris.append(n)
    stack=[]
    for node in graph.adjacents(n):
        stack.append(node)
        visited[node]
     

def BFS(n):
    '''semplice BFS'''
    q=queue.Queue()
    q.put(n)
    while(not q.empty()):
        node=q.get()
        if(not visited[node]):
            print(node)
            visited[node]=True
            for adj in graph.adjacents(node):
                if(not visited[adj]):
                    q.put(adj)
           
        

def CC_alberi():
    '''Restituisce il numero di componenti connesse del grafo'''
    count=0
    for i in graph.nodes():
        if(not visited[i] and not DFS_cicli(i)):
            count+=1            
    return count
    


def conta_cicli():
    '''Restituisce il numero di cicli presenti nel grafo'''
    count=0
    for i in graph.nodes():
        if(not visited[i] and DFS_cicli(i)):
            count+=1
    return count
    

def DFS_cicli(n):
    '''Metodo d'appoggio per contare i cicli,
    da true se esiste un ciclo visitando a partire dal nodo n'''
    visited[n]=-1
    for i in graph.adjacents(n):
        if(not visited[i]):
            if(DFS_cicli(i)):
                return True
        elif(visited[i]==1):
            return True
    visited[n]=1
    return False
    


def DFS_ponti():
    '''metodo che stampa tutti i ponti'''
    for i in graph.nodes():
        if(not visited[i]):
            DFS_ponti_app(i)
    return app_sol
   
#liste utili per il metodo d'appoggio     
app_sol=[]
id_count=0    
id_arr=ll=len(graph.nodes())*[-1]

def DFS_ponti_app(n):
    '''Metodo d'appoggio per stampare i ponti'''
    global id_count
    visited[n]=1
    id_arr[n]=ll[n]=id_count
    id_count+=1
    for i in graph.adjacents(n):
        if(parent[n]==i):
            continue
        if(not visited[i]):
            parent[i]=n
            DFS_ponti_app(i)
            ll[n]=min(ll[n],ll[i])      #aggiorno il ll nel callback
            if(id_arr[n]<ll[i]):
                app_sol.append((i,n))
        else:
            ll[n]=min(ll[n],id_arr[i])      #trovo il nodo piu in alto che posso raggiungere
    return app_sol



art=set()
def punti_art():
    '''Algoritmo per trovare i punti d'articolazione'''
    global id_count
    for i in graph.nodes():
        if(not visited[i]):
            print(i)
            punti_art_app(i)
            id_count=0

def punti_art_app(n):
    '''Metodo d'appoggio dell'algoritmo per trovare i punti d'articolazione'''
    global id_count
    visited[n]=ll[n]=id_count=id_count+1
    figli=0
    for i in graph.adjacents(n):
        if(parent[n]==i):
            continue
        if(not visited[i]):
            figli+=1
            parent[i]=n
            punti_art_app(i)
            ll[n]=min(ll[n],ll[i])
            if(ll[i]>=visited[n] and parent[n]!=-1):
                print(n,i)
                art.add(n)
        else:
            ll[n]=min(ll[n],visited[i])
    if(parent[n]==-1 and figli>=2):
        art.add(n)
