import java.util.HashSet;

public class Vertex {
	private int value;

	private HashSet<Vertex> adjacent;
	
	public Vertex(int value) {
		adjacent=new HashSet<Vertex>();
		this.value=value;
	}
	
	public void connect(Vertex ver) {
		adjacent.add(ver);
	}
	
	
	public int getValue() {
		return value;
	}

	public String toString() {
		return value+"";
	}
	
	public HashSet<Vertex> getAdjacent() {
		return adjacent;
	}
	
	
}
	
