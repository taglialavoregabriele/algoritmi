import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

@SuppressWarnings("unused")
public class DirectGraph {
	private ArrayList<Vertex> vertexes = new ArrayList<>();
	private int lastNode = 0;
	private HashSet<Vertex> visited = new HashSet<>();
	private int padri[] = new int[99];

	public void addVertex() {
		vertexes.add(new Vertex(lastNode));
		lastNode++;
	}

	public void connect(Vertex in, Vertex out) {
		vertexes.get(vertexes.indexOf(in)).connect(out);
	}

	public void connect(int in, int out) {
		connect(getVertex(in), getVertex(out));
	}

	public Vertex getVertex(int vertex) {
		return vertexes.get(vertex);
	}

	public ArrayList<Vertex> getVertexes() {
		return vertexes;
	}

	public String toString() {
		String app = "";
		for (Vertex v : vertexes) {
			app = app + v.getValue() + " -> ";
			for (Vertex adj : v.getAdjacent()) {
				app = app + adj.getValue() + " ";
			}
			app = app + "\n";
		}
		return app;
	}

	public void dfs(Vertex v) {
		System.out.println(v);
		visited.add(v);
		for (Vertex ver : v.getAdjacent()) {
			if (!visited.contains(ver)) {
				padri[ver.getValue()] = v.getValue();
				dfs(ver);
			}
		}
	}

	// INIZIO
	private ArrayList<Vertex> app = new ArrayList<>();
	public boolean attivi[] = new boolean[99];

	private ArrayList<Vertex> cicloApp(Vertex v) {
		visited.add(v);
		attivi[v.getValue()] = true;
		for (Vertex ver : v.getAdjacent()) {
			if (attivi[ver.getValue()]) {
				app.add(v);
				app.add(ver);
				return app;
			}
			if (!visited.contains(ver)) {
				visited.add(ver);
				attivi[ver.getValue()] = true;
				padri[ver.getValue()] = v.getValue();
				app = cicloApp(ver);
				// if(app.isEmpty() || app==null) return null;
			}
		}

		attivi[v.getValue()] = false;
		return app;
	}

	private ArrayList<Vertex> app2 = new ArrayList<>();

	public ArrayList<Vertex> dammiUnCiclo() {
		app2 = cicloApp(vertexes.get(0));
		if (app2.isEmpty())
			return app2;
		Vertex uno = app2.get(0);
		Vertex due = app2.get(1);
		app2.clear();
		while (uno.getValue() != due.getValue()) {
			app2.add(uno);
			uno = vertexes.get(padri[uno.getValue()]);
		}
		app2.add(due);
		return app2;
	}
	// FINE

	public HashSet<Vertex> ponti() {
		// TODO
		return null;
	}

	
	//INIZIO
	public boolean visitati[] = new boolean[99];

	public boolean ciclo(Vertex x) {
		visitati[x.getValue()] = true;
		attivi[x.getValue()] = true;
		for (Vertex v : x.getAdjacent()) {
			if (attivi[v.getValue()])
				return true;
			if (!visitati[v.getValue()]) {
				attivi[v.getValue()] = true;
				if (ciclo(v))
					return true;
			}
		}
		attivi[x.getValue()] = false;

		return false;
	}

	public void topOrder() {
		if (vertexes.isEmpty() || ciclo(vertexes.get(0))) {
			System.out.println("è presente un ciclo");
			return;
		}

		for (int i = 0; i < visitati.length; i++) {
			visitati[i] = false;
		}

		for (Vertex v : vertexes) {
			if (!visitati[v.getValue()]) {
				dfsOrd(v);
			}
		}
		Collections.reverse(appTop);
		for (Vertex v : appTop) {
			System.out.print(v + " ");
		}

	}

	public ArrayList<Vertex> appTop = new ArrayList<>();

	public void dfsOrd(Vertex v) {		
		visitati[v.getValue()] = true;
		for (Vertex j : v.getAdjacent()) {
			if (!visitati[j.getValue()]) {
				visitati[j.getValue()] = true;
				dfsOrd(j);
			}
		}
		appTop.add(v);

	}
	//FINE
	
	
	//INIZIO
	int avanti=0;
	int indietro=0;
	int attraversamento=0;
	public void archi() {
		for (int i = 0; i < visitati.length; i++) {
			visitati[i] = false;
		}
		dfsArchi(vertexes.get(3));
//		for(Vertex v:vertexes) {
//			if(!visitati[v.getValue()]) dfsArchi(v);
//		}
		System.out.println(" avanti "+avanti+" indietro "+indietro+" attraversamento "+attraversamento);
	}
	
	public void dfsArchi(Vertex v) {
		avanti++;
		visitati[v.getValue()]=true;
		attivi[v.getValue()]=true;
		for(Vertex ver:v.getAdjacent()) {
			if(attivi[ver.getValue()]) {
				indietro++;
			}else if(visitati[ver.getValue()]) {
				attraversamento++;
			}else {
				dfsArchi(ver);
			}
		}
		attivi[v.getValue()]=false;
		
	}
	//FINE
	
	
	public static void main(String[] args) {
		Random r = new Random();
		DirectGraph dg = new DirectGraph();
		for (int i = 0; i < 6; i++)
			dg.addVertex();

		dg.connect(0, 4);
		dg.connect(3, 4);
		dg.connect(4, 1);
		dg.connect(1, 0);
		dg.connect(1, 5);
		dg.connect(4, 5);
		dg.connect(2, 5);
		dg.connect(1, 2);
		System.out.println(dg);
		// for(int i=0;i<15;i++)
		// dg.connect(dg.getVertex(r.nextInt(6)), dg.getVertex(r.nextInt(6)));
		dg.archi();

	}
}

