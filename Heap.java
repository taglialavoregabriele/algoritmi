//classe che crea un Heap da un array ed esegue l'heapSort
public class Heap {

	private int arr[];
	private int heapSize;
	
	public Heap(int [] arr) {
		this.arr=arr.clone();
		heapSize=this.arr.length;
		buildHeap();
	}
	
	public void buildHeap() {
		for(int i=arr.length/2-1;i>=0;i--) heapify(i);
	}
	
	public void heapify(int i) { 
		int l=i*2+1,r=l+1;
		int indMax=i;
		if(l<heapSize && arr[l]>arr[indMax]) indMax=l;
		if(r<heapSize && arr[r]>arr[indMax]) indMax=r;
		if(indMax!=i) {
			int app=arr[i];
			arr[i]=arr[indMax];
			arr[indMax]=app;
			heapify(indMax);
		}
	}
	
	public void heapSort() {
		buildHeap();
		heapSize--;
		while(heapSize>1) {
			int app=arr[0];
			arr[0]=arr[heapSize];
			arr[heapSize]=app;
			heapSize--;
			heapify(0);
		}
		heapSize=arr.length;
	}
	
	//TODO aggiungi valore, rimuovi valore, etc...
	
	@Override
	public String toString() {
		String app="";
		for(int i:arr) app+=i+" ";
		return app;
	}
	
	public static void main(String[]args) {
		int arr[] = {120, 11, 93, 5, 88, 7, 18, 14, 234, 552, 109, 42};
		Heap heap=new Heap(arr);
		System.out.println(heap);
		heap.heapSort();
		System.out.println(heap);
	}
}
