public class GruppoSomma
{
    /*
    dato un numero ed una lista
    ritorna vero e stampa i valori necessari
    se è possibile ottenere quel numero dalla somma dei valori di un qualsiasi sottoinsieme della lista fornita,
    ritorna falso altrimenti
    */
    public static boolean gruppoSomma(int n, int[] values) {
		int tot = 0;
		for (int i = 0; i < values.length; i++)
			tot += values[i];
		if (tot < n)
			return false;
		if (n == tot) {
			for (int cont = 0; cont < values.length; cont++) {
				System.out.print(values[cont] + " ");
			}
			System.out.println();
			return true;
		}
		if (values.length > 1) {
			int[] app = new int[values.length - 1];
			for (int i = 0; i < values.length; i++) {
				for (int j = 0; j < app.length; j++) {
					if (i > j)
						app[j] = values[j];
					else
						app[j] = values[j + 1];
				}
				if (gruppoSomma(n, app))
					return true;
			}
		}
		return false;
	}
    
	public static void main(String[] args) {
		int[] arr={3,15,-2,-6,-2,5};
		gruppoSomma(7,arr);
	}
}
