public class PermutazioniDispariPari
{
    
    //lunghezza massima sequenza
	static int len = 20;
    //array statico utilizzato per semplicità, da rifare con ArrayList
	static int sol[] = new int[len];
    //array che tiene conto dei valori già presi
    static boolean preso[] = new boolean[len];

    /*
    stampa tutte le permutazioni di numeri da 1 a n dove i valori si alternano tra dispari e pari
    */
	private static void stampaPermDispPariApp(int n, int i) {
	    
		if (i > n) {
			for (int j = 0; j < n; j++)
				System.out.print(sol[j] + " ");
			System.out.println();
			return;
		}

		for (int j = 1; j <= n; j++) {
			if (!preso[j]) {
				if ((i - 1) % 2 == 0) {
					if (j % 2 != 0) {
						sol[i - 1] = j;
						preso[j] = true;
						stampaPermDispPariApp(n, i + 1);
						preso[j] = false;
					}
				} else {
					if (j % 2 == 0) {
						sol[i - 1] = j;
						preso[j] = true;
						stampaPermDispPariApp(n, i + 1);
						preso[j] = false;
					}
				}
			}
		}
	}
	
    //metodo che fa partire la ricorsione
	public static void stampaPermDispPari(int n){
	    stampaPermDispPariApp(n,1);
	}
    
    
	public static void main(String[] args) {
		stampaPermDispPari(5);
	}
}
