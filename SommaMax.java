public class SommaMax
{
    //array dove viene eseguito l'algoritmo
    static int[] array = { 1, -12, 1, 4, -4, 2, 0, -1, 2, 3, 3, -7, 5, 10, 1, -2 };

    /*
    data una lista e due puntatori 
    restituisce il valore massimo ottenibile
    tramite la somma di valori consecutivi
    contenuti dentro il range
    */
	public static int sommaMax(int a, int b) {
        //TODO controllo dell'input
		if (a == b) {
			if (array[a] > 0)
				return array[a];
			else
				return 0;
		}
		int m = (a + b) / 2;
		int max1 = sommaMax(a, m);
		int max2 = sommaMax(m + 1, b);
		int pre = 0;
		int sum = 0;
		for (int i = m; i >= a; i--) {
			sum += array[i];
			if (sum > pre)
				pre = sum;
		}
		int suf = 0;
		sum = 0;
		for (int i = m + 1; i <= b; i++) {
			sum += array[i];
			if (sum > suf)
				suf = sum;
		}
		for (int j = a; j <= b; j++)
			System.out.print(array[j] + " ");
		System.out.println(
				"    max1: " + max1 + " max2: " + max2 + " pre: " + pre + " suf: " + suf + " a: " + a + " b: " + b);
		m = Math.max(max1, Math.max(max2, pre + suf));
		return m;
	}
    
	public static void main(String[] args) {
		
		System.out.println(sommaMax(0,array.length-1));
	}
}

