public class MinSquares
{
    
    /*
    metodo che ritorna il numero minimo di quadrati con cui è possibile rappresentare un numero
    esempi:
    per 9 il risultato è 1 (9=3^2)
    per 5 il risultato è 2 (5=2^2 + 1^2)
    per 7 il risultato è 4 (7=2^2 + 1^2 + 1^2 + 1^2)
    */
    public static int getMinSquares(int n) {

		// casi base, quindi resetituita immediatamente
		if (n <= 3)
			return n;

		// Creiamo una lista per calcolare le soluzioni dinamicamente
		int dp[] = new int[n + 1];

		// casi base
		dp[0] = 0;
		dp[1] = 1;
		dp[2] = 2;
		dp[3] = 3;

		// riempiamo il resto della lista
		for (int i = 4; i <= n; i++) {
			// il numero massimo di quadrati è sempre i perchè si può sempre rappresentare un numero come somma di 1^2
			dp[i] = i;

			// Formula utilizzata per riempire la lista
			for (int x = 1; x <= i; x++) {
				int temp = x * x;
				if (temp > i)
					break;
				else
					dp[i] = Math.min(dp[i], 1 + dp[i - temp]);
			}
		}

		// mettiamo il risultato dentro res
		int res = dp[n];

        //stampa tutta la lista
		//for (int j = 0; j < dp.length; j++)
		//	System.out.println(j + " : " + dp[j]);
		//System.out.println();
		
        return res;
	}
    
    
	public static void main(String[] args) {
		System.out.println(getMinSquares(9));
	}
}
