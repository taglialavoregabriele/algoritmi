# Algoritmi

Collezione di algoritmi svolti durante l'università

## Note

Questi algoritmi sono stati svolti con lo scopo di risolvere i problemi indicati in maniera efficiente, non mi sono curato troppo della qualità del codice poichè lo scopo era semplicemente esercitarmi a risolvere algoritmi
