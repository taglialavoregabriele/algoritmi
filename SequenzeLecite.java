import java.util.HashSet;
public class SequenzeLecite
{
    //variabile d'appoggio statica
    public static int count = 0;

    /*
    in questa classe sono presenti 2 metodi
    uno conta e l'altro stampa tutte le sequenze lecite lunghe N
    una sequenza è lecita se inizia con un numero da 1 a M e se ogni numero è un divisore del precedente
    */

	private static int contaSequenzeLeciteApp(int n, int m, int k) {

		if (k > n) {
			return count += 1;
		}

		HashSet<Integer> est = new HashSet<>();

		if (k == 1)
			for (int i = m; i >= 1; i--)
				est.add(i);

		else
			for (int i = m; i >= 1; i--)
				if (m % i == 0)
					est.add(i);

		for (int j : est) {
			contaSequenzeLeciteApp(n, j, k + 1);
		}
		return count;
	}
	
    //lunghezza massima sequenza
	static int len = 20;
    //array statico utilizzato per semplicità, da rifare con ArrayList
	static int sol[] = new int[len];

	private static void stampaSequenzeLeciteApp(int n, int m, int i) {

		if (i > n) {
			for (int j = 0; j < len; j++)
				System.out.print(sol[j] + " ");
			System.out.println();
			return;
		}

		HashSet<Integer> est = new HashSet<>();

		if (i > 1) {
			for (int j = m; j > 0; j--)
				if (m % j == 0)
					est.add(j);
		} else
			for (int k = 1; k <= m; k++)
				est.add(k);

		for (int j : est) {
			sol[i - 1] = j;
			stampaSequenzeLeciteApp(n, j, i + 1);
		}

	}
    
    //metodo che chiama contaSequenzeLeciteApp inizializzando k a 1
    public static void contaSequenzeLecite(int n, int m){
        contaSequenzeLeciteApp(n,m,1);
    }
    
    //metodo che chiama stampaSequenzeLeciteApp inizializzando i a 1
    public static void stampaSequenzeLecite(int n, int m){
        stampaSequenzeLeciteApp(n,m,1);
    }
    
    
	public static void main(String[] args) {
		//System.out.println(contaSequenzeLecite(1,2,1));
		stampaSequenzeLecite(5,10);
	}
}
