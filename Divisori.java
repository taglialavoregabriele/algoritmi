public class Divisori
{
    /*
    stampa tutti i divisori del numero dato in input
    in radice di N passi
    dove N è il numero dato in input
    */
    public static void divisors(int n) {
		HashSet<Integer> divisors = new HashSet<>();
		for (int i = 1; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				divisors.add(i);
				if (i != n / i)
					divisors.add(n / i);
			}
		}
		System.out.println(divisors);
	}
    
	public static void main(String[] args) {
		divisors(100);
	}
}
